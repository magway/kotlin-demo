import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

/*
buildscript {

    val springBootVersion = "2.7.18"
    val kotlinVersion = "1.9.22"
    repositories {
        maven {
            url = uri("https://rpm.ftc.ru/nexus/repository/remote-repos/")
        }
        maven {
            url = uri("https://rpm.ftc.ru/nexus/repository/libs-release")
        }
        maven {
            //noinspection HttpUrlsUsage
            url = uri("http://dpci-dev.ftc.ru:8081/artifactory/repo")
            isAllowInsecureProtocol = true
        }
    }

    dependencies {
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
        classpath("org.jetbrains.kotlin.jvm:org.jetbrains.kotlin.jvm.gradle.plugin:${kotlinVersion}")
        classpath("org.jetbrains.kotlin.plugin.spring:org.jetbrains.kotlin.plugin.spring.gradle.plugin:${kotlinVersion}")
    }

}


apply(plugin = "org.springframework.boot")
apply(plugin = "io.spring.dependency-management")
apply(plugin = "org.jetbrains.kotlin.jvm")
apply(plugin = "org.jetbrains.kotlin.plugin.spring")

*/

plugins{
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    kotlin("jvm")
    kotlin("plugin.spring")

}

/*
plugins {
    id("org.springframework.boot") version "2.7.18"
    id("io.spring.dependency-management") version "1.1.4"
    kotlin("jvm") version "1.9.22"
    kotlin("plugin.spring") version "1.9.22"
}
*/

group = "com.example"
version = "0.0.1-SNAPSHOT"


repositories {
    maven {
        url = uri("https://rpm.ftc.ru/nexus/repository/remote-repos/")
    }
    maven {
        url = uri("https://rpm.ftc.ru/nexus/repository/libs-release")
    }
    maven {
        //noinspection HttpUrlsUsage
        url = uri("http://dpci-dev.ftc.ru:8081/artifactory/repo")
        isAllowInsecureProtocol = true
    }
    mavenLocal()
    mavenCentral()
}

dependencies {

    implementation("org.springframework.boot:spring-boot-starter")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}


tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs += "-Xjsr305=strict"
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
