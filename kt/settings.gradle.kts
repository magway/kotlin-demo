rootProject.name = "ktdemo"

buildscript {
    val springBootVersion = "2.7.18"
    val kotlinVersion = "1.9.22"
    repositories {
        maven {
            url = uri("https://rpm.ftc.ru/nexus/repository/remote-repos/")
        }
        maven {
            url = uri("https://rpm.ftc.ru/nexus/repository/libs-release")
        }
        maven {
            //noinspection HttpUrlsUsage
            url = uri("http://dpci-dev.ftc.ru:8081/artifactory/repo")
            isAllowInsecureProtocol = true
        }
        mavenLocal()
        gradlePluginPortal()
    }

    dependencies{
        classpath("org.springframework.boot:spring-boot-gradle-plugin:${springBootVersion}")
        classpath("org.jetbrains.kotlin.jvm:org.jetbrains.kotlin.jvm.gradle.plugin:${kotlinVersion}")
        classpath("org.jetbrains.kotlin.plugin.spring:org.jetbrains.kotlin.plugin.spring.gradle.plugin:${kotlinVersion}")

    }
}