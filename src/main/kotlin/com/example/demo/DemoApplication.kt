package com.example.demo

import com.example.demo.config.BlogProperties
import org.springframework.boot.Banner
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean


@SpringBootApplication
@EnableConfigurationProperties(BlogProperties::class)
class DemoApplication {
    @Bean
    fun commandLineRunnerBean() = CommandLineRunner { args: Array<String?> ->
        println("CommandLineRunner: " + args.joinToString(",", "[", "]"))
    }
}

/**
 * entry point to app
 * @return nothing
 * @param args - run parameters
 */
fun main(args: Array<String>) {
    runApplication<DemoApplication>(*args) {
        setBannerMode(Banner.Mode.OFF)
    }
}
