package com.example.demo.config

import com.example.demo.controller.dto.serializing.JacksonAnnotationIntrospectorVersionRule
import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Configuration
class JacksonCustomizer {

    /**
     * default configuration for Object mapper
     * it's possible (but not mandatory) to use this approach for configure mapper used by Spring
     */
    @Bean
    @Primary
    fun objectMapper(): ObjectMapper = ObjectMapper().adjustMapperForKotlin()
        .setAnnotationIntrospector(JacksonAnnotationIntrospectorVersionRule(3))

    private fun ObjectMapper.adjustMapperForKotlin() =
        registerKotlinModule()
            .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
            .setVisibility(PropertyAccessor.CREATOR, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE)
            .setVisibility(PropertyAccessor.IS_GETTER, JsonAutoDetect.Visibility.NONE)

    /**
     * default configuration for jackson mapper customizer
     * it's possible (but not mandatory) to use this approach for configure default mapper annotation processor and so on
     */
    @Bean
    fun jackson2ObjectMapperBuilderCustomizer() = Jackson2ObjectMapperBuilderCustomizer {
        it.annotationIntrospector(JacksonAnnotationIntrospectorVersionRule(3))
    }
}
