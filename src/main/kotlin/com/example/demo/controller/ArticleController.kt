package com.example.demo.controller

import com.example.demo.controller.dto.ArticleDto
import com.example.demo.controller.dto.toDto
import com.example.demo.dao.ArticleRepository
import com.example.demo.model.Article
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/article")
class ArticleController(private val repository: ArticleRepository) {

    @Operation(summary = "get article list", description = "retrieve all articles")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "everething is ok"),
            ApiResponse(responseCode = "400", description = "some troubles")
        ]
    )
    @GetMapping("", "/")
    fun findAll(): Iterable<ArticleDto> =
        repository.findAllByOrderByAddedAtDesc().map(Article::toDto)

    @GetMapping("/{slug}")
    fun findOne(@PathVariable slug: String): ArticleDto =
        repository.findBySlug(slug)?.toDto() ?: throw ResponseStatusException(
            HttpStatus.NOT_FOUND,
            "This article does not exist"
        )

}

