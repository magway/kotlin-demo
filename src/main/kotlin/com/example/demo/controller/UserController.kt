package com.example.demo.controller

import com.example.demo.controller.dto.UserDto
import com.example.demo.controller.dto.serializing.JacksonAnnotationIntrospectorVersionRule
import com.example.demo.controller.dto.toDto
import com.example.demo.dao.UserRepository
import com.example.demo.model.User
import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.PropertyAccessor
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/api/user")
class UserController(private val repository: UserRepository) {

    @GetMapping("/")
    fun findAll(): Iterable<UserDto> =
        repository.findAll().map(User::toDto)

    @GetMapping("/v")
    fun findAll(@RequestParam("version", required = true) version: Int): String {
        val users = repository.findAll().map(User::toDto)
        val objectMapper = createObjectMapper()
        objectMapper.setAnnotationIntrospector(JacksonAnnotationIntrospectorVersionRule(version))
        val writeValueAsString = objectMapper.writeValueAsString(users)
        return writeValueAsString
    }

    @GetMapping("/{login}")
    fun findOne(@PathVariable login: String) =
        repository.findByLogin(login)?.toDto() ?: throw ResponseStatusException(
            HttpStatus.NOT_FOUND,
            "This user does not exist"
        )

    private fun createObjectMapper(): ObjectMapper = ObjectMapper().registerKotlinModule()
        .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY)
        .setVisibility(PropertyAccessor.CREATOR, JsonAutoDetect.Visibility.NONE)
        .setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.NONE)
        .setVisibility(PropertyAccessor.SETTER, JsonAutoDetect.Visibility.NONE)
        .setVisibility(PropertyAccessor.IS_GETTER, JsonAutoDetect.Visibility.NONE)
}