package com.example.demo.controller.dto

import com.example.demo.extensions.toSlug
import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDateTime
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Schema(name = "Article", description = "Some Article")
data class ArticleDto(
    @field:[Size(min = 1, max = 500) NotBlank]
    val title: String,

    @field:NotNull
    val headline: String,

    val content: String,

    @field:NotNull
    val author: UserDto,

    val slug: String = title.toSlug(),

    val addedAt: LocalDateTime = LocalDateTime.now(),

    val id: Long? = null
)
