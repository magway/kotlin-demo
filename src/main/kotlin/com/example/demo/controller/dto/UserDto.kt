package com.example.demo.controller.dto

import com.example.demo.controller.dto.serializing.VersionRule
import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonIgnore

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
data class UserDto(
    val login: String,

    @VersionRule(apiVersionTo = 3, ignore = true)
    @VersionRule(apiVersionFrom = 3, apiVersionTo = 6, fieldName = "V3_6_name")
    @VersionRule(apiVersionFrom = 7, fieldName = "V7_name")
    @VersionRule(apiVersionFrom = 9, ignore = true)
    val firstname: String,

    @VersionRule(apiVersionTo = 3, ignore = true)
    val lastname: String,

    @JsonIgnore
    val description: String? = null,

    @JsonIgnore
    val id: Long? = null
)
