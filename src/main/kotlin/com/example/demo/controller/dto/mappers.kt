package com.example.demo.controller.dto

import com.example.demo.model.Article
import com.example.demo.model.User

fun Article.toDto() = ArticleDto(title, headline, content, author.toDto(), slug, addedAt, id)

fun User.toDto() = UserDto(login, firstname, lastname, description, id)