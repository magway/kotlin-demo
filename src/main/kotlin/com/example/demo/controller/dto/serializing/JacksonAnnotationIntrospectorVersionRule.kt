package com.example.demo.controller.dto.serializing

import com.fasterxml.jackson.databind.PropertyName
import com.fasterxml.jackson.databind.introspect.Annotated
import com.fasterxml.jackson.databind.introspect.AnnotatedMember
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector

class JacksonAnnotationIntrospectorVersionRule(val apiVersion: Int) :
    JacksonAnnotationIntrospector() {

    override fun findNameForSerialization(a: Annotated?): PropertyName? =
        findPropertyName(a) ?: super.findNameForSerialization(a)

    override fun findNameForDeserialization(a: Annotated?): PropertyName? =
        findPropertyName(a) ?: super.findNameForDeserialization(a)

    override fun hasIgnoreMarker(m: AnnotatedMember?): Boolean {
        val ret = findRule(m)?.ignore ?: super.hasIgnoreMarker(m)
        println(" ${m?.name ?: "xxxx"}- ${if (ret) "ignore" else ""}")
        return ret
    }

    private fun findRule(a: Annotated?): VersionRule? {
        return a?.annotated?.getAnnotationsByType(VersionRule::class.java)?.let { annotations ->
            annotations.filter {
                (it.apiVersionFrom == -1 || it.apiVersionFrom <= apiVersion)
                        && (it.apiVersionTo == -1 || it.apiVersionTo >= apiVersion)
            }
                .maxByOrNull { it.apiVersionFrom }
        }
    }

    private fun findPropertyName(a: Annotated?): PropertyName? =
        findRule(a)?.let { PropertyName(it.fieldName) }

}
