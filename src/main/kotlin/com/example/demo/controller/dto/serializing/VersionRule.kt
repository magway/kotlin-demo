package com.example.demo.controller.dto.serializing

import com.fasterxml.jackson.annotation.JacksonAnnotation

@Repeatable
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
@MustBeDocumented
@JacksonAnnotation
annotation class VersionRule(
    val apiVersionFrom: Int = -1,
    val apiVersionTo: Int = -1,
    val fieldName: String = "",
    val ignore: Boolean = false
)
